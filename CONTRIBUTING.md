## Contribute to OpenTAP

Thank you for your interest in contributing to OpenTAP. Contributions includes feature enhancement request, issue reporting, bug fixing... 

This guide details how to contribute to OpenTAP in a way that is efficient for everyone.

## Contributor license agreement

OpenTAP requires contributors to sign a contribution license agreement (CLA). 
You can find the CLA [here](http://opentap.io/docs/OpenTAP%20Contributor%20License%20Agreement%2020190515.pdf).
The CLA can be signed by an individual or a company. For companies, please add the company name and have a legal authorized representative endorsing the document.

*  By submitting code as an individual you agree to the
individual contributor license agreement.

*  By submitting code as an entity you agree to the
corporate contributor license agreement.

After reception of the endorsed CLA, we will promote you as a developer in the OpenTAP project.

## Submitting a merge request

We welcome merge requests with fixes and improvements to OpenTAP code or documentation. 

In the issue tracker, the label `Accepting Merge Requests` denotes issues that we agree is a good idea, and would really like merge requessts for but other improvements are also most welcome.

Issues labeled `To be Discussed` needs futher discussions. Try to resolve these discussions, and get the label removed, before starting to work on these issues. 

Merge requests should be opened at https://gitlab.com/OpenTAP/opentap.